const express = require('express')
const router = express.Router();
const userController = require('../controllers/userController')


router.post('/register',userController.register);
router.post('/login',userController.login);
router.post('/changepassword',userController.changePassword);
router.post('/forgot',userController.forgotPassword)
router.post('/reset',userController.resetPassword);

router.get('/',userController.getAllUsers);
router.get('/:id/details',userController.getUserDetails);

router.put('/setAdmin',userController.setAdmin);

module.exports = router;
