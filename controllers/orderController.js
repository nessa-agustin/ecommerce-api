const Orders = require('../models/Order')
const auth = require('../auth')



async function createOrder(req,res){

    const tokenData = await auth.decode(req.headers.authorization);

    const isAdmin = tokenData.isAdmin;
    const userId = tokenData.id;
    let reqBody = req.body;

    if(!isAdmin){

        let newOrder = new Orders(reqBody);
        newOrder.userId = userId;

        try {
            let checkout = await newOrder.save();
            return res.send(checkout)   
        } catch (error) {
            console.log(error)
            return res.status(500).send(false)
        }

    }else{
        return res.status(401).send(false)
    }

}


async function getOrders(req,res){

    const tokenData = await auth.decode(req.headers.authorization);

    const isAdmin = tokenData.isAdmin;
    const userId = tokenData.id;
    let filter = req.query.filter;
    let findParams = {};

    if(filter == 'all'){
        
    }else if(filter == 'completed'){
        findParams.isCompleted = true
    }else{
        findParams.isCompleted = false
    }

    if(!isAdmin){
        findParams.userId = userId
    }

    let orderDocs = await Orders.find(findParams)

    return res.send(orderDocs)

}



module.exports = {
    createOrder: createOrder,
    getOrders: getOrders
}